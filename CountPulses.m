function [ n ] = CountPulses( noiseLevel, aux_m )
%CountPulses Counts the number of pulses
%   and suppresses interferance
for i = 1:1074
    for k=0:999; 
        aux_n = aux_m(1000*k+1:1000*(k+1)); 
        %   We reduce the size of the analyzed piece of the file to 1000 ps,
        %   the loop will be repeated until the whole part of the file has been analyzed.
        if sum(aux_n() < (noiseLevel - 3))== 0;
            n = 0;
            %   If the value in the analyzed piece of the file has a value lower than the noise level, 
            %   it means that it is a distortion and there is no peak in this fragment.
        else 
            n = 1;
            %   Otherwise the peak occurs and we include it in the final result.
        end
        clear aux_n;
        %   In order not to overload the variable, we remove its value each time the loop is executed.
    end
end
end