function [ n ] = NoiseLevel( dane )
%NoiseLevel Finds the noise level inregistered signal
%   These lines of code find the maximum value in the 'dane' matrix and enter its value.
%   The 'dane' matrix is a matrix in which occurrences of numbers in the analyzed file are counted.
    [~, b] = find(dane == max(dane));
    n = b;
end