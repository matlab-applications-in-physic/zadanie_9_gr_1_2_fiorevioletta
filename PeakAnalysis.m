function [] = PeakAnalysis( MeasurmentData ) % The function gets the name of the analyzed file.
%PeakAnalysis The function analyzes data from a large file
%   Program written in MatLab
%   Creator: Julia Pluta
%   All Contents 2019 Copyright  Julia Pluta All rights reserved
% Enter the place where the analyzed file is in the auxiliary variable and open it.
aux_0 = ['C:\Users\student\Desktop\MatLab_Pluta\', MeasurmentData];
field = fopen(aux_0, 'r');

% We set constants needed to divide the file into the analyzed areas 
% and add a matrix from which we will construct the histogram.
blockSize = 107374182; 
dane = zeros(1, 255);
pulsesSize = 1073741; 

% In this loop, we analyze the whole file piece by piece.
% Each individual value is read and the value +1 is added in place of its value in the 'dane' matrix.
for i = 1: 80
    aux_n = fread(field, blockSize, 'uint8');
    for k = 1 : blockSize   
        dane(aux_n(k)) = dane(aux_n(k)) + 1;
    end;
    % We clean the contents of the variable so as not to overload it.
    clear aux_n;
end;

% We create a chart, add axis descriptions and title. 
% Then save it to a file with the same name as the file being analyzed.
Plot = plot(dane);
xlabel('Samples');
ylabel('Level');
title('Histogram');
aux_6 = [MeasurmentData, '_histogram.pdf'];
saveas(Plot, aux_6);

% We create a global variable for the value of the noise level in the analyzed file
% and write to it the value of the noise level calculated from the NoiseLevel function.
global noiseLevel;
noiseLevel = NoiseLevel(dane);

% We create a global variable for the number of peaks.
global globalPulses;
globalPulses = 0;
% Each individual value is read
% and values from repeated use of CountPulse are added to the global variable.
for i = 1: 80
    aux_m = fread(field, pulsesSize, 'uint8');
    countPulses = CountPulses(noiseLevel, aux_m);
    globalPulses = globalPulses + countPulses;
    clear aux_m;
end;

% The program will write to the desktop the name of the file being analyzed, 
% the noise level and the number of counted peaks.
fprintf('Name of the analyzed file: ');
fprintf(MeasurmentData);
fprintf('Noise Level: %d \n', noiseLevel);
fprintf('Count Pulses: %d \n', globalPulses);

% The program creates and saves in the file with the name starting with the name of the analyzed file, 
% the name of the analyzed file, the noise level and the number of counted peaks.
aux_5 = [MeasurmentData, '_peak_analysis_results.dat'];
fid = fopen(aux_5 ,'w');
fprintf(fid, 'Noise Level: %d \n', noiseLevel);
fprintf(fid, 'Count Pulses: %d \n', countPulses);
fprintf(fid, 'Name of the analyzed file: ');
fprintf(fid, MeasurmentData);
fclose(fid);
end